# Fedora legal documentation

## License

Documentation maintained in this repository is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License (SPDX: CC-BY-SA-4.0). Certain other material may be under other terms.

